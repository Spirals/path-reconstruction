# Path reconstruction from points

## Setup

The notebooks contained in this repository require some dependencies to be installed on your local machine (usually using `pip install X`):

* bokeh
* colour
* jupyterlab
* matplotlib
* numpy
* pandas
* "psycopg[binary,pool]"

We highly recommand you to install those dependencies in a [virtual environment](https://packaging.python.org/en/latest/tutorials/installing-packages/#creating-virtual-environments) to avoid installing them in your userspace.

### GeoLife experiment

This notepad uses a spatial database to find points that are close to others; to run this experiment, you need to have a database container running, as described in the instructions below.

#### PostgreSQL+PostGIS database

```shell
# Create container
sudo docker run --name geolife-postgis -e POSTGRES_DB=geolife_database -e POSTGRES_PASSWORD=password -e POSTGRES_USER=geolife_user -p 55432:5432 -v ./database.sql:/docker-entrypoint-initdb.d/initDbContainer.sql -d postgis/postgis:15-3.3-alpine

# Connect to the database if you want to
PGPASSWORD=password psql -U geolife_user -h 127.0.0.1 -p 55432 -d geolife_database
```

#### To go further

* Bring back all dataset traces to the same day
* Take transport into account
* Try to build segments from points to reduce complexity?
