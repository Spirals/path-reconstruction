GRANT ALL PRIVILEGES ON DATABASE geolife_database TO geolife_user;

CREATE TABLE points (
    id              serial primary key,
    coordinates     geometry(point),
    timestamp       timestamptz,
    used            boolean DEFAULT FALSE
);
GRANT ALL PRIVILEGES ON TABLE points TO geolife_user;
ALTER TABLE points OWNER TO geolife_user;
